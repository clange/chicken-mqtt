(import mosquitto-mqtt)
(import (chicken blob))

(define mqttc (mqtt-new #f #t))

(define (message-callback topic payload)
  (display topic)
  (display "/")
  (display (blob->string payload))
  (display "\n"))

(define (connect-callback return-code)
  (display "Connection return code: ")
  (display return-code)
  (display "\n")
  (if (= return-code 0)
      (mqtt-subscribe mqttc "greetings/#" 0)))

(mqtt-set-callbacks mqttc `((message ,message-callback)
                            (connect ,connect-callback)))

(display (mqtt-connect mqttc "localhost" 1883 60))

(mqtt-loop-forever mqttc -1 1)
