;; to compile module manually:
;; csc -I /usr/include -L "-l mosquitto" -j mosquitto-mqtt -s mosquitto-mqtt.scm
;; csc -I /usr/local/Cellar/mosquitto/1.6.7/include -L "-l mosquitto" -j mosquitto-mqtt -s mosquitto-mqtt.scm

(module mosquitto-mqtt
  (mosquitto-lib-cleanup
   mosquitto-lib-version
   mosquitto-destroy
   mosquitto-publish
   mosquitto-loop-forever
   mosquitto-subscribe
   mosquitto-unsubscribe

   mqtt-new
   mqtt-connect
   mqtt-reconnect
   mqtt-set-callbacks
   mqtt-publish
   mqtt-subscribe
;   mqtt-unsubscribe
;   mqtt-destroy
   mqtt-loop
   mqtt-loop-forever)


  (import scheme)
  (import (chicken memory)
          (chicken foreign)
          (chicken blob))
  (import bind)

  (bind-rename/pattern "_" "-")

  (bind-file* "mosquitto-mqtt.api")

  ;; initialize mosquitto mqtt -- ignore return value which
  ;; is always 'success'
  (mosquitto-lib-init)


  ;; 'meta' callback functions: reads Scheme callback from mqtt
  ;; client userdata

  (define-external (message_meta_callback ((c-pointer "struct mosquitto") mosq)
                                          ((c-pointer void) userdata) 
                                          ((c-pointer "struct mosquitto_message") msg))
                    void

		  (let* ((len (mosquitto-message-payloadlen msg))
		         (*payload (mosquitto-message-payload msg))
             (payload (make-blob len))
		         (mid (mosquitto-message-mid msg))
		         (topic (mosquitto-message-topic msg))
		         (qos (mosquitto-message-qos msg))
		         (retain (mosquitto-message-retain msg))
             (callback-entry (assq 'message (get-gc-ref userdata)))
             (callback (if callback-entry (cadr callback-entry) #f)) )
		         ;; int mid; char* topic; int qos; bool retain

		     (if (> len 0)
		       (move-memory! *payload payload len))

         (if callback
           (callback topic payload)) ))


  (define-external (connect_meta_callback ((c-pointer "struct mosquitto") mosq)
                                     ((c-pointer void) userdata)
                                     (int rc))
    void

    (let* ( (callback-entry (assq 'connect (get-gc-ref userdata)))
            (callback (if callback-entry (cadr callback-entry) #f)) )

      (if callback 
          (callback rc)) ))


  ;; helper functions for dealing with C / garbage collection

  (define new-gc-root
    (foreign-lambda* c-pointer ()
      "C_return(CHICKEN_new_gc_root());\n"))

  (define set-gc-ref
    (foreign-lambda* void ((c-pointer root) (scheme-object reference))
      "CHICKEN_gc_root_set(root, reference);\n"))

  (define get-gc-ref
    (foreign-lambda* scheme-object ((c-pointer root))
      "C_return(CHICKEN_gc_root_ref(root));\n"))



  ;; -- public interface
 
  ;; register new mqtt instance _and_ install 'meta' callback functions
  ;; 'callback-gc-root' will be passed on each call of a callback in 'userdata',
  ;; content is an alist with callbacks
  ;; 'id': string as client id or #f; clean-session, see mosquitto docs
  (define (mqtt-new id clean-session)
    (let*  ((callback-gc-root (new-gc-root))
            ; 'clean_session' must be #t if id is #f (see mosquitto docs):
            (mqttc (mosquitto-new id (if (not id) #t clean-session) callback-gc-root)))

      (mosquitto-message-callback-set mqttc (location message_meta_callback))
      (mosquitto-connect-callback-set mqttc (location connect_meta_callback))

      (list mqttc callback-gc-root) ))

  ;; set callbacks (in an alist w/ keys 'message' and 'connect') for client 'mqttc'
  (define (mqtt-set-callbacks mqttc callbacks)
    (set-gc-ref (cadr mqttc) callbacks))

  ;; connect to broker
  (define (mqtt-connect mqttc host port timeout)
    (mosquitto-connect (car mqttc) host port timeout))

  ;; subscribe to topic
  ;; returns error code and 'mid': message ID
  (define (mqtt-subscribe mqttc topic qos)
    (mosquitto-subscribe (car mqttc) topic qos))

  ;; operate loop tasks; call regularly! see 'mosquitto_loop'
  (define (mqtt-loop mqttc timeout max-packets)
    (mosquitto-loop (car mqttc) timeout max-packets))

  ;; reconnect after connection loss
  (define (mqtt-reconnect mqttc)
    (mosquitto-reconnect (car mqttc)))

  ;; publish 'payload', a blob (use '(blob->string payload)' for strings)
  (define (mqtt-publish mqttc topic payload qos retain)
    (mosquitto-publish (car mqttc) topic payload qos retain))

  ;; loop forever, reconnect if connection lost
  (define (mqtt-loop-forever mqttc timeout max-packets)
    (let loop ()
      (if (not (= (mqtt-loop mqttc timeout max-packets) 0))
          (mqtt-reconnect mqttc))
      (loop)) )

) ; end module
