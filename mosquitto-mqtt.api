struct mosquitto_message{
  int mid;
  char *topic;
  void *payload;
  int payloadlen;
  int qos;
  ___bool retain;
};

int mosquitto_lib_init();
int mosquitto_lib_cleanup();
int mosquitto_lib_version(___out int *major,
			  ___out int *minor,
			  ___out int *revision);
struct mosquitto *mosquitto_new(const char *id,
                                ___bool clean_session,
				void *obj);
void mosquitto_destroy(struct mosquitto *mosq);
int mosquitto_connect(struct mosquitto *mosq,
		      const char *host,
		      int port,
       	      int keepalive);
int mosquitto_reconnect(struct mosquitto *mosq);
int mosquitto_publish(struct mosquitto *mosq,
                       ___out int *mid,
 		      const char *topic,
 		      ___length(payload) int payloadlen,
 		      ___blob payload /* const void *payload */,
 		      int qos,
 		      ___bool retain);

___safe int mosquitto_loop(struct mosquitto *mosq, int timeout, int max_packets);
___safe int mosquitto_loop_forever(struct mosquitto * mosq, int timeout, int max_packets);

int mosquitto_subscribe(struct mosquitto *mosq,
 			___out int *mid,
 			const char *sub,
 			int qos);
int mosquitto_unsubscribe(struct mosquitto *mosq,
                                  int *mid,
				  const char *sub);

/* setting callbacks */
void mosquitto_connect_callback_set(struct mosquitto *mosq,
                                    void (*on_connect)(struct mosquitto *, void *, int));

/* NB: the last type of return value in the vallback is 'const struct *' according
       to mosquitto.h; however, it seems impossible to encode this in the
       Scheme callback function using combinations of 'c-pointer' and 'const' */
void mosquitto_message_callback_set(struct mosquitto *mosq,
                                    void (*on_message)(struct mosquitto *,
				             	       void *,
						       struct mosquitto_message *));
